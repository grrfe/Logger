package fe.logger

import java.time.LocalDateTime
import java.time.ZoneOffset

data class LogEntry(val logger: Logger,
                    val dateTime: LocalDateTime,
                    val type: Logger.Type,
                    val message: String) {

    val timestamp = dateTime.toEpochSecond(ZoneOffset.UTC) * 1000

    fun print() {
        println("[%s][%s @ %s] %s".format(type.toString(),
                logger.prefix, dateTime.format(logger.dtf), message))
    }
}