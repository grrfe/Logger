package fe.logger;

import org.jetbrains.annotations.Nullable;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * A simple prefixed-logger
 * <p>
 * Output format: [type][prefix @ datetime] message
 */
public class Logger {
    private DateTimeFormatter dtf = DEFAULT_DTF;

    /**
     * The prefix that should be used for this instance
     */
    private final String prefix;

    /**
     * Determines if the stacktrace of an exception should be printed
     */
    private boolean exceptionStacktrace;

    /**
     * The format of the date and time in the output
     */
    public static final DateTimeFormatter DEFAULT_DTF = DateTimeFormatter.ISO_LOCAL_DATE_TIME;

    private boolean instanceDisabled;

    private static boolean globalDisabled;
    public static final List<LogEntry> LOG_ENTRIES = new LinkedList<>();
    public final List<LogEntry> entries = new LinkedList<>();

    /**
     * Creates a new logger using the given prefix
     *
     * @param prefix              the prefix the logger should use
     * @param exceptionStacktrace determines if the stacktrace of an exception should be printed when given to {@link #print(Type, String, Object...)}
     */
    public Logger(String prefix, boolean exceptionStacktrace) {
        this.prefix = prefix;
        this.exceptionStacktrace = exceptionStacktrace;
    }

    public Logger(String prefix) {
        this.prefix = prefix;
    }

    public Logger(String prefix, boolean exceptionStacktrace, DateTimeFormatter dtf) {
        this.prefix = prefix;
        this.exceptionStacktrace = exceptionStacktrace;
        this.dtf = dtf;
    }


    public void setInstanceDisabled(boolean instanceDisabled) {
        this.instanceDisabled = instanceDisabled;
    }

    public static void setGlobalDisabled(boolean globalDisabled) {
        Logger.globalDisabled = globalDisabled;
    }

    /**
     * Print a message
     *
     * @param type    the logger type that should be used (for the output)
     * @param message the message that should be printed
     */
    public Logger print(Type type, String message) {
        if (Logger.globalDisabled || this.instanceDisabled) {
            return this;
        }

        LogEntry le = new LogEntry(this, LocalDateTime.now(), type, message);
        le.print();

        this.entries.add(le);
        LOG_ENTRIES.add(le);

        return this;
    }

    /**
     * A convenience wrapper for {@link #print(Type, String)} which accepts the same formatting as {@link String#format(String, Object...)}
     *
     * @param type    the logger type that should be used (for the output)
     * @param message the message that should be printed (can contain %s, %d, etc.)
     * @param args    the arguments which should be filled into the placeholders (refer to {@link String#format(String, Object...)})
     */
    public Logger print(Type type, String message, Object... args) {
        if (Logger.globalDisabled || this.instanceDisabled) {
            return this;
        }

        if (this.exceptionStacktrace) {
            for (int i = 0; i < args.length; i++) {
                if (args[i] instanceof Exception) {
                    args[i] = this.getStringFromException((Exception) args[i]);
                }
            }
        }

        return this.print(type, String.format(message, args));
    }

    public Logger print(Type type, Exception e) {
        return this.print(type, this.getStringFromException(e));
    }

    private String getStringFromException(Exception e) {
        StringWriter sw = new StringWriter();
        e.printStackTrace(new PrintWriter(sw));

        return sw.toString();
    }

    public DateTimeFormatter getDtf() {
        return dtf;
    }

    @Nullable
    public Object getPrefix() {
        return prefix;
    }

    /**
     * Available logger types
     */
    public enum Type {
        SUCCESS("S"), WARNING("W"), ERROR("E"), INFO("I");

        private final String shortCode;

        Type(String shortCode) {
            this.shortCode = shortCode;
        }

        public String getShortCode() {
            return shortCode;
        }
    }
}