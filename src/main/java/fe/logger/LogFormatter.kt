package fe.logger

object LogFormatter {
    @JvmStatic
    fun formatAll(): String {
        val lines = mutableListOf<FormattedLogger>()
        var lastFormatted: FormattedLogger? = null

        Logger.LOG_ENTRIES.forEach { le ->
            if (lastFormatted == null || le.logger != lastFormatted?.logger) {
                lastFormatted = FormattedLogger(le.logger).also {
                    lines.add(it)
                }
            }

            lastFormatted?.entries?.add(le)
        }

        return this.createString(lines)
    }

    @JvmStatic
    fun format(logger: Logger) = this.createString(listOf(FormattedLogger(logger).also {
        it.entries.addAll(logger.entries)
    }))


    private fun createString(lines: List<FormattedLogger>) = buildString {
        lines.forEach { line ->
            this.append(line.logger.prefix).append("\n")
            line.entries.forEach { entry ->
                this.append("\t")
                        .append("%s @ %d: %s".format(entry.type.shortCode, entry.timestamp, entry.message))
                        .append("\n")
            }
        }
    }

    data class FormattedLogger(val logger: Logger) {
        val entries = mutableListOf<LogEntry>()
    }
}